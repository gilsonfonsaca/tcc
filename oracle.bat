cls
echo Iniciando Processo > z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Criando Objetos >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlplus dwuser/qaplo002@ORCL @Z:\dados_olap\sql\DDL_OLAP_FINAL_ORACLE.sql >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela CIDADE >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D1_CIDADE.ctl log=Z:\dados_olap\log\CIDADE.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela SEXO >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D2_SEXO.ctl log=Z:\dados_olap\log\SEXO.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela ESCOLARIDADE >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D3_ESCOLARIDADE.ctl log=Z:\dados_olap\log\ESCOLARIDADE.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela ESTCIVIL >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D4_ESTCIVIL.ctl log=Z:\dados_olap\log\ESTCIVIL.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela RENDA >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D5_RENDA.ctl log=Z:\dados_olap\log\RENDA.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela CANAL >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D6_CANAL.ctl log=Z:\dados_olap\log\CANAL.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela PRODUTO >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D7_PRODUTO.ctl log=Z:\dados_olap\log\PRODUTO.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela ENTREGA >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D8_ENTREGA.ctl log=Z:\dados_olap\log\ENTREGA.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela AVALIACAO >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\D9_AVALIACAO.ctl log=Z:\dados_olap\log\AVALIACAO.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela PERIODO >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\T0_PERIODO.ctl log=Z:\dados_olap\log\PERIODO.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Tabela Fato >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
sqlldr dwuser/qaplo002@ORCL control=Z:\dados_olap\ctl\F0_FATO.ctl log=Z:\dados_olap\log\fato1.log SILENT=(FEEDBACK) >> z:\dados_olap\log\oracle.log
date /T >> z:\dados_olap\log\oracle.log
time /T >> z:\dados_olap\log\oracle.log
echo Import Finalizado >> z:\dados_olap\log\oracle.log
echo ------------------------------------- >> z:\dados_olap\log\oracle.log
exit