options (errors=9999999, rows=2000)
load data
 characterset UTF8
 infile 'Z:\dados_olap\csv\D7_PRODUTO.csv'
 badfile 'Z:\dados_olap\err\D7_PRODUTO.err'
 discardfile 'Z:\dados_olap\err\D7_PRODUTO.dis'
 into table D7_PRODUTO
 fields terminated by ";"
( ID, MARCA, MODELO, VALOR_UNIT )