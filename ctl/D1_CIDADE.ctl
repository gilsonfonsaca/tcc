options (errors=9999999, rows=2000)
load data
 characterset UTF8
 infile 'Z:\dados_olap\csv\D1_CIDADE.csv'
 badfile 'Z:\dados_olap\err\D1_CIDADE.err'
 discardfile 'Z:\dados_olap\err\D1_CIDADE.dis'
 into table D1_CIDADE
 fields terminated by ";"
( ID, ESTADO, CIDADE, POPULACAO )