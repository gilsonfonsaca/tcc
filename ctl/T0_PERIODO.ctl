options (errors=9999999, rows=2000)
load data
 characterset UTF8
 infile 'Z:\dados_olap\csv\T0_PERIODO.csv'
 badfile 'Z:\dados_olap\err\T0_PERIODO.err'
 discardfile 'Z:\dados_olap\err\T0_PERIODO.dis'
 into table T0_PERIODO
 fields terminated by ";"
( ID, DATA_COMPRA date 'YYYY-MM-DD' )