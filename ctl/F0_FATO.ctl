options (errors=9999999, rows=2000)
load data
 characterset UTF8
 infile 'Z:\dados_olap\csv\F0_FATO_2.csv'
 badfile 'Z:\dados_olap\err\F0_FATO_2.err'
 discardfile 'Z:\dados_olap\err\F0_FATO_2.dis'
 into table F0_FATO
 fields terminated by ";"
( ID, ID_PERIODO, ID_CIDADE, ID_SEXO, ID_ESCOLARIDADE, ID_ESTCIVIL, ID_RENDA, ID_CANAL, ID_PRODUTO, ID_ENTREGA, ID_AVALIACAO,
    QTDADE_COMPRA, VALOR_UNIT, VALOR_FRETE, VALOR_IMPOSTO, VALOR_TOTAL )