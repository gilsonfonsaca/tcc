options (errors=9999999, rows=2000)
load data
 characterset UTF8
 infile 'Z:\dados_olap\csv\D3_ESCOLARIDADE.csv'
 badfile 'Z:\dados_olap\err\D3_ESCOLARIDADE.err'
 discardfile 'Z:\dados_olap\err\D3_ESCOLARIDADE.dis'
 into table D3_ESCOLARIDADE
 fields terminated by ";"
( ID, ESCOLARIDADE )